#include <stdio.h>	
#include "partida.h"

void inicializar_partida(t_partida *partida)
{	
	partida->jugadores.n_jgd = preguntar_n_en_rango("¿Numero de jugadores?", MIN_JGDS, MAX_JGDS);
	partida->jugadores.humano = preguntar_si_o_no("¿Un jugador humano?");
	partida->omnic = preguntar_si_o_no("¿Quieres omniconsciencia?");
	printf("\n");

	inicializar_pila(&partida->pila);
	desordenar_pila(&partida->pila);
	inicializar_jugadores(&partida->jugadores, partida->jugadores.n_jgd, partida->jugadores.humano, &partida->pila);
	inicializar_mesa(&partida->jugadores, &partida->mesa);
}

void pasar_turno(t_partida *partida)
{
	if(partida->jugadores.turno != partida->jugadores.n_jgd-1)
		partida->jugadores.turno++;
	else
		partida->jugadores.turno = 0;
		
	partida->num_pasadas++;
}

void imprimir_turno(t_jugadores jugadores)
{
	printf("Turno: %d\n", jugadores.turno);
}

void realizar_jugada(t_partida *partida)
{
	int tir_ele, continua = 0;

	/* printf_color(GREEN_BOLD); */
	
	do{
		tiradas_posibles(partida->mesa, partida->jugadores.jgd[partida->jugadores.turno], &partida->tiradas);
		
		if(partida->tiradas.n_tiradas != 0)
			continua = 1;
		else {
			if(partida->pila.n_fichas != 0)
				sacar_ficha(&partida->pila, &partida->jugadores.jgd[partida->jugadores.turno]);
			else
				pasar_turno(partida);
		}
	} while (!continua);
	
	if(partida->jugadores.jgd[partida->jugadores.turno].humano == HUMANO) {
		imprimir_tiradas(partida->tiradas);
		tir_ele = preguntar_n_en_rango("¿Que tirada juegas?", 0, partida->tiradas.n_tiradas - 1); 
	} else {
		tir_ele = 0;
		printf("Tira: ");
		imprimir_tirada(partida->tiradas.tirada[0]);
	}
	
	poner_ficha_mesa(&partida->mesa, &partida->tiradas.tirada[tir_ele]);
	eliminar_ficha(partida->jugadores.jgd[partida->jugadores.turno].ficha, &partida->jugadores.jgd[partida->jugadores.turno].n_fichas, partida->jugadores.jgd[partida->jugadores.turno].pos_posible_tirada[tir_ele]);
	
	printf("\n\n");
}

void imprimir_partida(t_partida partida)
{
	imprimir_pila(partida.pila);
	
	/* printf_color(WHITE_BOLD); */
	imprimir_mesa(partida.mesa);
	
	/* printf_color(RED_BOLD); */
	imprimir_jugadores(partida.jugadores);
	
	/* printf_color(BLUE_BOLD); */
	imprimir_turno(partida.jugadores);
	
	/* printf_reset_color(); */
}

int partida_acabada(t_partida partida)
{
	int estado;
	
	if(partida.jugadores.jgd[partida.jugadores.turno].n_fichas == 0) { 
		/* printf_color(BLUE_BOLD); */
		printf("===>>> HAS GANADO! :-) <<<===\n\n");
		/* printf_reset_color(); */
		estado = 1;
	} else
		estado = 0;
	
	return estado;
}