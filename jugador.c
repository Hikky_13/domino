#include <stdio.h>	
#include "jugador.h"

void inicializar_jugador(t_jugador *jugador, int humano, int num_j, t_pila *pila)
{
	int i;

	jugador->n_fichas = FICHAS_JUGADOR;
	jugador->humano = humano;
	jugador->num_j = num_j;
	
	for (i = 0; i < FICHAS_JUGADOR; i++) {
		jugador->ficha[i] = pila->ficha[i];
		eliminar_ficha(pila->ficha, &pila->n_fichas, i);
	}
}	

void sacar_ficha(t_pila *pila, t_jugador *jugador)
{
	jugador->ficha[jugador->n_fichas].c1 = pila->ficha[pila->n_fichas - 1].c1;
	jugador->ficha[jugador->n_fichas].c2 = pila->ficha[pila->n_fichas - 1].c2;
	
	printf("Vas a la pila! Pillas un ");
	imprimir_ficha(jugador->ficha[jugador->n_fichas]);
	printf(" ;-P\n");
	
	jugador->n_fichas++;
	eliminar_ficha(pila->ficha, &pila->n_fichas, pila->n_fichas - 1);
}

void imprimir_jugador(t_jugador jugador, int num_j)
{
	int i;

	printf("J%d:     ", num_j);
	
	for (i = 0; i < jugador.n_fichas; i++)
		imprimir_ficha(jugador.ficha[i]);

	if (jugador.humano == HUMANO)
		printf("Humano\n");
	else
		printf("Robot\n");
}

void inicializar_jugadores(t_jugadores *jugadores, int num_jugadores, int hum_rob, t_pila *pila)
{
	int i; 
	
	if (hum_rob == HUMANO)
		inicializar_jugador(&jugadores->jgd[0], 1, 0, pila);
	else
		inicializar_jugador(&jugadores->jgd[0], 0, 0, pila);
		
	for(i = 1; i < num_jugadores; i++)
		inicializar_jugador(&jugadores->jgd[i], 0, i, pila);
}

void imprimir_jugadores(t_jugadores jugadores)
{
	int i;
	
	for(i = 0; i < jugadores.n_jgd; i++)
		imprimir_jugador(jugadores.jgd[i], jugadores.jgd[i].num_j);
}

