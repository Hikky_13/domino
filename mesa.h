#ifndef MESA_H 
#define	MESA_H

#include "ficha.h"
#include "tiradas.h"

#define IZQUIERDA 1
#define DERECHA 0 
#define TRUE 1
#define FALSE 0


typedef struct{
	t_ficha ficha[MAX_FICHAS];
	int n_fichas;
}t_mesa;

void inicializar_mesa(t_jugadores *jugadores, t_mesa *mesa);
void poner_ficha_mesa(t_mesa *mesa, t_tirada *tirada);
void tiradas_posibles(t_mesa mesa, t_jugador jugador, t_tiradas *tiradas);
void imprimir_mesa(t_mesa mesa);

#endif
