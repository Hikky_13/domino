#ifndef PILA_H 
#define	PILA_H

#include "ficha.h"
#include "azar.h"

#define MAX_N_CARA 7
#define MAX_FICHAS 28

typedef struct{
	t_ficha ficha[MAX_FICHAS];
	int n_fichas;
}t_pila;

void inicializar_pila(t_pila *pila); 
void desordenar_pila(t_pila *pila);
void imprimir_pila(t_pila pila);

#endif
