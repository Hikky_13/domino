#include <stdio.h>	
#include "ficha.h"

void inicializar_ficha(int c1, int c2, t_ficha *f)
{	
	f->c1 = c1;
	f->c2 = c2;
}

void imprimir_ficha(t_ficha ficha)
{
	printf("%d:%d|", ficha.c1, ficha.c2);
}

void girar_ficha(t_ficha *ficha)
{
	int aux;
	
	aux = ficha->c1;
	ficha->c1 = ficha->c2;
	ficha->c2 = aux;
}

void eliminar_ficha(t_ficha *vector, int *nelem, int pos)
{
	int i;
	
	for (i = pos; i < *nelem - 1; i++)
		vector[i] = vector[i+1];
	(*nelem)--;
}
