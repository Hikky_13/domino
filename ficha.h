#ifndef FICHA_H 
#define	FICHA_H

typedef struct{
	int c1, c2;
}t_ficha;

void inicializar_ficha(int c1, int c2, t_ficha *f);
void imprimir_ficha(t_ficha ficha);
void girar_ficha(t_ficha *ficha);
void eliminar_ficha(t_ficha *vector, int *nelem, int pos);

#endif
