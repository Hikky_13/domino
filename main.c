#include <stdio.h>

#include "ficha.h"
#include "azar.h"
#include "mesa.h"
#include "pila.h"
#include "tiradas.h"
#include "jugador.h"
#include "partida.h"
#include "colores.h"
#include "preguntas.h"


int main ()
{
	inicializar_azar(); /* Esto tiene que ponerse al principio para inicializar la generaci�n de n�meros aleatorios */
	
	t_partida pa;

	inicializar_partida(&pa);
	do{
		pasar_turno(&pa);
		imprimir_partida(pa);
		realizar_jugada(&pa);
	} while(!partida_acabada(pa));
	imprimir_partida(pa);
}
