#include <stdio.h>	
#include "mesa.h"

void inicializar_mesa(t_jugadores *jugadores, t_mesa *mesa)
{
	int i, j, num_dma = -1, jug_dma = -1, posf_dma = -1;
	
	/* Buscamos el doble mas alto */
	
	for (i = 0; i < jugadores->n_jgd; i++) {      /*recorre jugadores*/
		for (j = 0; j < jugadores->jgd[i].n_fichas; j++) {       /*recorre fichas del jugador*/
			if (jugadores->jgd[i].ficha[j].c1 == jugadores->jgd[i].ficha[j].c2 && jugadores->jgd[i].ficha[j].c1 > num_dma) {
				num_dma = jugadores->jgd[i].ficha[j].c1;
				jug_dma = i; 
				posf_dma = j;
			}
		}
	}
	
	if(num_dma == -1) {
		mesa->ficha[0].c1 = jugadores->jgd[0].ficha[0].c1;
		mesa->ficha[0].c2 = jugadores->jgd[0].ficha[0].c2;
		eliminar_ficha(jugadores->jgd[0].ficha, &jugadores->jgd[0].n_fichas, 0);
		jugadores->turno = 0;
	} else {
		mesa->ficha[0].c1 = num_dma;
		mesa->ficha[0].c2 = num_dma;
		eliminar_ficha(jugadores->jgd[jug_dma].ficha, &jugadores->jgd[jug_dma].n_fichas, posf_dma);
		jugadores->turno = jug_dma;
	}	
	
	mesa->n_fichas = 1;
}

void poner_ficha_mesa(t_mesa *mesa, t_tirada *tirada)
{
	int i;

	if(tirada->girada == TRUE)
		girar_ficha(&tirada->ficha);
	
	if(tirada->extremo == IZQUIERDA) {
		for (i = mesa->n_fichas - 1; i >= 0 ; i--)
			mesa->ficha[i+1] = mesa->ficha[i];
		mesa->ficha[0] = tirada->ficha;
	} else {
		mesa->ficha[mesa->n_fichas] =  tirada->ficha;
	}	
	mesa->n_fichas++;
}	

void tiradas_posibles(t_mesa mesa, t_jugador jugador, t_tiradas *tiradas)
{
	int i;
	tiradas->n_tiradas = 0;	

	for(i = 0; i < jugador.n_fichas; i++) {               
		if(jugador.ficha[i].c2 == mesa.ficha[0].c1) {      
			tiradas->tirada[tiradas->n_tiradas].ficha.c1 = jugador.ficha[i].c1;
			tiradas->tirada[tiradas->n_tiradas].ficha.c2 = jugador.ficha[i].c2; 
			tiradas->tirada[tiradas->n_tiradas].extremo = IZQUIERDA;
			tiradas->tirada[tiradas->n_tiradas].girada = FALSE;
			jugador.pos_posible_tirada[tiradas->n_tiradas] = i;
			tiradas->n_tiradas++;
		}
		else if(jugador.ficha[i].c1 == mesa.ficha[mesa.n_fichas-1].c2) {  
			tiradas->tirada[tiradas->n_tiradas].ficha.c1 = jugador.ficha[i].c1;
			tiradas->tirada[tiradas->n_tiradas].ficha.c2 = jugador.ficha[i].c2; 
			tiradas->tirada[tiradas->n_tiradas].extremo = DERECHA;
			tiradas->tirada[tiradas->n_tiradas].girada = FALSE;
			jugador.pos_posible_tirada[tiradas->n_tiradas] = i;
			tiradas->n_tiradas++;
		} 
		else if(jugador.ficha[i].c1 == mesa.ficha[0].c1) {     
			tiradas->tirada[tiradas->n_tiradas].ficha.c1 = jugador.ficha[i].c1;
			tiradas->tirada[tiradas->n_tiradas].ficha.c2 = jugador.ficha[i].c2; 
			tiradas->tirada[tiradas->n_tiradas].extremo = IZQUIERDA;
			tiradas->tirada[tiradas->n_tiradas].girada = TRUE;
			jugador.pos_posible_tirada[tiradas->n_tiradas] = i;
			tiradas->n_tiradas++;
		}
		else if(jugador.ficha[i].c2 == mesa.ficha[mesa.n_fichas-1].c2) {   
			tiradas->tirada[tiradas->n_tiradas].ficha.c1 = jugador.ficha[i].c1;
			tiradas->tirada[tiradas->n_tiradas].ficha.c2 = jugador.ficha[i].c2;
			tiradas->tirada[tiradas->n_tiradas].extremo = DERECHA;
			tiradas->tirada[tiradas->n_tiradas].girada = TRUE;
			jugador.pos_posible_tirada[tiradas->n_tiradas] = i;
			tiradas->n_tiradas++;
		}
	}

}

void imprimir_mesa(t_mesa mesa) 
{
	int i; 

	printf("Mesa:   ");
	for (i = 0; i < mesa.n_fichas; i++)
		imprimir_ficha(mesa.ficha[i]);
	printf("\n");
}
