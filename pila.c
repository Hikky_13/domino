#include <stdio.h>	
#include "pila.h"

void inicializar_pila(t_pila *pila) 
{
	int i, j, num_ficha = 0;	

	pila->n_fichas = MAX_FICHAS;
	
	for (i = 0; i < MAX_N_CARA; i++) { 
		for (j = i; j < MAX_N_CARA; j++) {
			inicializar_ficha(j, i, &pila->ficha[num_ficha]);
			num_ficha++;
		}
	}
}

void desordenar_pila(t_pila *pila)
{
	int na1, na2, aux, i; 
	
	for (i = 0; i < 1000; i++) {
		na1 = numero_al_azar(MAX_FICHAS);
		na2 = numero_al_azar(MAX_FICHAS);
	
		aux = pila->ficha[na1].c1;
		pila->ficha[na1].c1 = pila->ficha[na2].c1;
		pila->ficha[na2].c1 = aux;
		
		aux = pila->ficha[na1].c2;
		pila->ficha[na1].c2 = pila->ficha[na2].c2;
		pila->ficha[na2].c2 = aux;
	}
}

void imprimir_pila(t_pila pila) 
{	
	int i;	
	
	printf("Pila:   ");
	
	for (i = 0; i < pila.n_fichas; i++) {
		imprimir_ficha(pila.ficha[i]);
	}
	
	printf("\n");
}

