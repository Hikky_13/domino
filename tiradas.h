#ifndef TIRADAS_H 
#define	TIRADAS_H

#include "ficha.h"
#include "jugador.h"

#define TRUE 1
#define FALSE 0
#define IZQUIERDA 1
#define DERECHA 0 

typedef struct{
	t_ficha ficha;
	int extremo; /*IZQUIERDA DERECHA*/
	int girada; /*TRUE FALSE*/
}t_tirada;

typedef struct{
	t_tirada tirada[MAX_FICHAS];
	int n_tiradas;
}t_tiradas;

void imprimir_tirada(t_tirada tirada);
void imprimir_tiradas(t_tiradas tiradas);

#endif
