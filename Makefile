TARGET = dt.exe
OBJS = main.o azar.o ficha.o jugador.o mesa.o partida.o pila.o tiradas.o colores.o preguntas.o

all: $(TARGET)

$(TARGET): $(OBJS)
	gcc -o dt.exe $^
	
%.o: %.c
	gcc -c -o $@ $<
	
clean:
	rm -f $(TARGET) $(OBJS)
	