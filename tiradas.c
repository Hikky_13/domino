#include <stdio.h>	
#include "tiradas.h"

void imprimir_tirada(t_tirada tirada) /* d3:5| o i3:5| etc */
{
	if (tirada.extremo == IZQUIERDA)		
		printf("i");
	else 
		printf("d");

	imprimir_ficha(tirada.ficha);
}

void imprimir_tiradas(t_tiradas tiradas) /* Tiradas posibles: 0(d3:5|) 1(d2:4|)  ... */
{
	int i;

	printf("Tiradas posibles: ");
	
	for (i = 0; i < tiradas.n_tiradas; i++) {
		printf("%d(", i);
		imprimir_tirada(tiradas.tirada[i]);
		printf(")  ");
	}
	printf("\n");
}
