#ifndef JUGADOR_H 
#define	JUGADOR_H

#include "ficha.h"
#include "pila.h"

#define MAX_JUGADORES 4
#define FICHAS_JUGADOR 7
#define HUMANO 1
#define ROBOT 0 


typedef struct{
	t_ficha ficha[MAX_FICHAS];
	int pos_posible_tirada[MAX_FICHAS];
	int n_fichas;
	int humano;
	int num_j; /* numero identificador de cada jugador*/ 
}t_jugador;

typedef struct{
	t_jugador jgd[MAX_JUGADORES];
	int n_jgd; /* numero de jugadores */
	int turno;
	int humano;
}t_jugadores;

void inicializar_jugador(t_jugador *jugador, int humano, int num_j, t_pila *pila);
void sacar_ficha(t_pila *pila, t_jugador *jugador);
void imprimir_jugador(t_jugador jugador, int num_j);

void inicializar_jugadores(t_jugadores *jugadores, int num_jugadores, int hum_rob, t_pila *pila);
void imprimir_jugadores(t_jugadores jugadores);

#endif
