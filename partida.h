#ifndef PARTIDA_H 
#define	PARTIDA_H

#include "tiradas.h"
#include "mesa.h"
#include "jugador.h"
#include "colores.h"
#include "preguntas.h"

#define WHITE_BOLD 0 
#define RED_BOLD 1 
#define BLUE_BOLD 1 
#define GREEN_BOLD 1 

#define MIN_JGDS 2 
#define MAX_JGDS 4 

typedef struct{
	t_jugadores jugadores;
	t_tiradas tiradas;
	t_pila pila;
	t_mesa mesa;
	int omnic; /*TRUE FALSE*/
	int num_pasadas; 
}t_partida;

void inicializar_partida(t_partida *partida);
void pasar_turno(t_partida *partida);
void imprimir_turno(t_jugadores jugadores);
void realizar_jugada(t_partida *partida);
int partida_acabada(t_partida partida);
void imprimir_partida(t_partida partida);

#endif
